FROM openjdk:17-slim
COPY /target/*.jar /home/app/v47api.jar
ENTRYPOINT ["java", "-jar" , "/home/app/v47api.jar", "com.example.v47API.V47ApiApplication"]