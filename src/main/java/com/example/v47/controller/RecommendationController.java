package com.example.v47.controller;

import com.example.v47.model.Recommendation;
import com.example.v47.repository.RecommendationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

@RestController
@RequestMapping("/api/recommendations")
public class RecommendationController {

    @Autowired
    RecommendationRepository recommendationRepository ;

    @GetMapping()
    public List<Recommendation> getRecommendations(){
        return  recommendationRepository.findAll(); 
    }

    @GetMapping("/{productId}")
    public List<Recommendation> getRecommendationForProduct(@PathVariable long productId){
        return recommendationRepository.findByProductId(productId);
    }

    @PostMapping()
    public void addRecommendation(@RequestBody Recommendation recommendation){
        System.out.println(recommendation.toString());

        recommendationRepository.save(recommendation);

    }
}
