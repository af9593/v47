package com.example.v47.model;


import lombok.*;

import javax.persistence.*;

@Entity
@Table(name ="RECOMMENDATION")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Recommendation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id ;

    @Column(name = "PRODUCT_ID")
    private long productId ;

    @Column(name = "PRODUCT_NAME")
    private String productName;

    @Column (name = "RECOMMENDATION")
    private String recommendation;

    @Column(name = "CUSTOMER_NAME")
    private String customerName;
}
