package com.example.v47.repository;

import com.example.v47.model.Recommendation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RecommendationRepository extends JpaRepository<Recommendation, Long> {
    //Find by product Id

    List<Recommendation> findByProductId(long productId);
}
