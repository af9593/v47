package com.example.v47;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class V47Application {

	public static void main(String[] args) {
		SpringApplication.run(V47Application.class, args);
	}

}
